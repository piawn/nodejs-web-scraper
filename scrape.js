const request = require('request-promise');
const cheerio = require('cheerio');
const fs = require('fs');
let facts = [];

(async () => {
    try {
        const url = 'https://bestlifeonline.com/useless-facts/';
        const response = await request(url);
        const $ = cheerio.load(response);

        // https://www.youtube.com/watch?v=xTxo83RtmPY
        $('.header-mod').each((index, element) => {
            const number = $(element).children().first().text();
            const fact = $(element).children().last().text();
            facts[index] = {number, fact};
        });

        const json = JSON.stringify(facts, null, 2);

        fs.writeFile('scrape.json', json, (err) => {
        if (err) throw err;
        console.log('File scrape.json has been updated!');
        });

    } catch (e) {
        console.log('Error: ', e);
    }
})();